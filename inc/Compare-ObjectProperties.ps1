function Compare-ObjectProperties {
<#
.SYNOPSIS
Compares the values of multiple properties between two objects.

.EXAMPLE
$ad1 = Get-ADUser amelia.mitchell -Properties *
$ad2 = Get-ADUser carolyn.quinn -Properties *
Compare-ObjectProperties $ad1 $ad2

.NOTES
    Initial code borrowed from:
    https://blogs.technet.microsoft.com/janesays/2017/04/25/compare-all-properties-of-two-objects-in-windows-powershell/

    Thank you, Jamie Nelson!
#>
    param (
        [PSObject]$ReferenceObject,
        [PSObject]$DifferenceObject,
        # Subset of properties to compare. Defaults to comparing ALL.
        $Properties
    )
    $objprops = $Properties
    if ($objprops -eq $null) {
        $objprops = @()
        $objprops += $ReferenceObject | Get-Member -MemberType Property,NoteProperty | Select-Object -ExpandProperty Name
        $objprops += $DifferenceObject | Get-Member -MemberType Property,NoteProperty | Select-Object -ExpandProperty Name
        $objprops = $objprops | Sort-Object | Select-Object -Unique
    }
    foreach ($objprop in $objprops) {
        $diff = Compare-Object $ReferenceObject $DifferenceObject -Property $objprop
        if ($diff) {            
            Write-Output (
                [PsCustomObject][ordered]@{
                    PropertyName = $objprop
                    RefValue = $diff | Where-Object {$_.SideIndicator -eq '<='} | Select-Object -ExpandProperty $objprop
                    DiffValue = $diff | Where-Object {$_.SideIndicator -eq '=>'} | Select-Object -ExpandProperty $objprop
                }
            )
        }        
    }
}
Export-ModuleMember -Function Compare-ObjectProperties
