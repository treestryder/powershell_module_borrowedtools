Set-StrictMode -Version Latest

# Fail at the first exception.
trap {}

Get-ChildItem "$PSScriptRoot/inc/*.psm1" | foreach { Import-Module $_ }
Get-ChildItem "$PSScriptRoot/inc/*.ps1" | foreach { . $_ }
